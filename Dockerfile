# Use an official OpenJDK runtime as a parent image
FROM openjdk:8-jre-alpine

# Set the working directory to /app
WORKDIR /app

# Copy the application JAR file into the container at /app
COPY target/api-gateway-0.0.1-SNAPSHOT.jar /app/api-gateway-0.0.1-SNAPSHOT.jar

# Expose port 8080 to the outside world
EXPOSE 80:9090

# Define environment variables
ENV JAVA_OPTS=""

# Run the application when the container starts
CMD ["java", "-jar", "api-gateway-0.0.1-SNAPSHOT.jar"]
lkol amiltilhom haka !!!
